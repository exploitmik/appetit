# Appetit

Veja o projeto online: [appetit](http://appetit.now.sh)

## Considerações

1. Comumente opto por criar um componente do zero quando o prazo é viável ou o style guide é bem específico. [CardsWrapper](https://github.com/exploitmik/planets-vue/blob/master/src/components/molecules/CardsWrapper.vue) é um exemplo. Usado no projeto [Planets Vue](https://planets-vue.now.sh), escrito com SCSS, CSS Custom Properties, BEM e Atomic Design. Porém, neste teste optei por utilizar o [Vuetify](https://vuetifyjs.com) devido a forte semelhança dos componentes com a interface a ser seguida. Também por ter a semântica predefinida e usar atributos que melhoram a acessibilidade, como os padrões [WAI-ARIA](https://www.w3.org/WAI/standards-guidelines/aria).

2. Na funcionalidade de controle da quantidade do produto, usei um componente criado anteriormente por mim: [MinusMaxNumber](https://github.com/exploitmik/my-vue-components/blob/master/MinusMaxNumber/MinusMaxNumber.vue) usando a abordagem ['css properties com rem para controle de em'](https://9elements.com/blog/sizing-blocks-or-areas-using-css-custom-properties) para controlar o tamanho do componente.

3. Para a rota 'novo pedido', ao invés do uso de páginas, preferi a abordagem de componentes dinâmicos. Pois, além da flexibilidade de comunicação através de eventos, também traz uma melhor experiência para o usuário (UX) quando os campos já foram preenchidos e é necessário retroceder uma etapa, e caso posteriormente tenha a necessidade de prosseguir, os campos permanecem preenchidos. Isso sem a necessidade de uma abordagem localStorage, sessionStorage ou IndexedDB, sendo necessário apenas o component `<keep-alive></keep-alive>`.

4. Na lista de produtos para venda, decidi usar um array de objetos que, por sua vez, contém os produtos relacionados com o tipo/categoria de produto. Poderia ter usado dois arrays: um para tipos/categorias e outro para os produtos e correlacioná-los através do índice, mas não seria interessante. Optei por um algoritmo mais elaborado para filtrar. Da mesma forma, utilizei o mesmo algoritmo para filtrar os pedidos.

5. Pensei em usar uma Fake API como JSON Server ou MirageJS para demonstrar o consumo e uso de promises, mas não o fiz. Entretando, tenho estudado uma [arquitetura](https://medium.com/canariasjs/vue-api-calls-in-a-smart-way-8d521812c322) para chamadas api bem interessante. Para algo mais simples utilizo o famoso 'services/api'.

## Tecnologias

- **Vue**: Framework JavaScript Reativo;
- **VueRouter**: Sistema de roteamento client-side;
- **Vuex**: Controle de estado global da aplicação baseado na arquitetura [Flux](https://facebook.github.io/flux/docs/in-depth-overview) e inspirado no Redux;
- **Vuetify**: Componentes UI baseados no Meterial Design da Google;
- **PWA**: Permite um melhor gerencimento de cache e até o uso offline-first;
- **Sass**: Pre-processador que incorpora ao CSS algumas funcionalidades de linguagens de programação.

## Como baixar

É necessário ter no mínimo a versão 8 do [NodeJS](https://nodejs.org/en/download) instalado e consequentemente o NPM (que vem junto com o Node quando instalado). Neste projeto, optei pelo [Yarn](https://yarnpkg.com), mas fique a vontade para usar o que lhe for conveniente.

Clone este repositório (caso queira clonar por linha de comando necessitará do [git](https://git-scm.com) instalado)

```
git clone https://exploitmik@bitbucket.org/exploitmik/appetit.git
```

Depois vá até a pasta clonada, por exemplo:

```
cd appetit/
```

## Como rodar

Dentro da pasta, os seguintes comandos estarão disponíveis:

### Instalar todas as dependências para o projeto funcionar

```
yarn install
```

### Compilar e executar um servidor de desenvolvimento com hot-reloading

```
yarn run serve
```

### Compilar e minificar para produção

```
yarn run build
```
