import Vue from "vue";

const formatMoney = value => {
  const valueInput = Number(value) || 0;
  return new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL"
  }).format(valueInput);
};

export default Vue.filter("formatMoney", formatMoney);
