import Vue from "vue";
import Vuex from "vuex";
import orderModule from "./modules/order";

Vue.use(Vuex);

const state = {};
const getters = {};
const actions = {};
const mutations = {};

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  modules: {
    order: orderModule
  },
  strict: process.env.NODE_ENV !== "production"
});
