const currentProduct = state => state.orderProduct;
const quantityProduct = state => state.orderProduct.quantity;
const hasClient = state => state.orderClient[0].title;
const currentClient = state => state.orderClient;

export default {
  currentProduct,
  quantityProduct,
  hasClient,
  currentClient
};
