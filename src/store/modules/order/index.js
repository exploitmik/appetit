import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const state = {
  orderProduct: {
    title: "",
    avatar: "",
    price: 0,
    quantity: 0,
    observations: ""
  },
  orderClient: [
    {
      id: "",
      title: "",
      avatar: ""
    }
  ]
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
