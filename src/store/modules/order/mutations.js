const SET_ORDER_PRODUCT = (state, product) => (state.orderProduct = product);
const SET_ORDER_CLIENT = (state, client) => (state.orderClient = client);
const RESET_STATE = state => {
  (state.orderProduct = {
    title: "",
    avatar: "",
    price: 0,
    quantity: 0,
    observations: ""
  }),
    (state.orderClient = [
      {
        id: "",
        title: "",
        avatar: ""
      }
    ]);
};

export default {
  SET_ORDER_PRODUCT,
  SET_ORDER_CLIENT,
  RESET_STATE
};
