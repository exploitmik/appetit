import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import NewOrder from "../views/NewOrder.vue";
import FeedbackDone from "../views/FeedbackDone.vue";
import Orders from "../views/Orders.vue";
import InfoOrder from "../views/InfoOrder.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "index",
    component: Login
  },
  {
    path: "/pedidos",
    name: "pedidos",
    component: Orders
  },
  {
    path: "/pedidos/:id",
    name: "pedidos-id",
    component: InfoOrder
  },
  {
    path: "/novo-pedido",
    name: "novo-pedido",
    component: NewOrder
  },
  {
    path: "/pedido-concluido",
    name: "feedback-done",
    component: FeedbackDone
  }
];

const router = new VueRouter({
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

export default router;
